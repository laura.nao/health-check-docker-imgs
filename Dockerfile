FROM debian:bullseye-slim

ENV DEBIAN_FRONTEND=noninteractive
RUN apt update -q
RUN apt install -y android-tools-mkbootimg fastboot
RUN apt install -y inetutils-ping ssh
RUN rm -rf /var/lib/apt/lists/*
